#!/bin/bash
set -e

# Updates ToxerSFOS translation TS files.

SELF=$(dirname `readlink -f $0`)

TRCMD=$1

QTVER=${QTVER:-'5.14.0'}
QTDIR=${QTDIR:-"${HOME}/Qt/${QTVER}/gcc_64"}

PATH=$QTDIR/bin:$PATH
TRBASE=$SELF/translation/toxer_desktop

declare -a LANGUAGES
LANGUAGES=(de fr)

function update(){
  lupdate $SELF/qml -locations none -extensions qml -ts ${TRBASE}_${1}.ts
}

function qm(){
  lrelease -compress -removeidentical -qm ${TRBASE}_${1}.qm ${TRBASE}_${1}.ts
}

function usage(){
  echo "Usage: ${0} <command>"
  echo ""
  echo "Available commands:"
  echo " up -> Runs lupdate (create/update TS files)."
  echo " qm -> Runs lrelease (create QM binaries)."
  echo " help -> Shows this help."
  echo ""
  echo "Available environment variables:"
  echo "QTVER -> the Qt version (is: ${QTVERSION})"
  echo "QTDIR -> the Qt location (is: ${QTDIR})"
  echo ""
  echo "Happy translating!"
  echo ""
}

# main
echo ""
for l in ${LANGUAGES[@]} ; do
  case "${TRCMD}" in
    'up') update $l ;;
    'qm') qm $l ;;
    'help') usage ; exit 0 ;;
    '') usage ; exit 1 ;;
    *) echo -e "\e[0;31mError: The command is not available!\e[0m\n"; usage ; exit 1 ;;
  esac
done
