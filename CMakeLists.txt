cmake_minimum_required(VERSION 3.23)
project(ToxerDesktop)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_MODULE_PATH ${ToxerDesktop_SOURCE_DIR}/cmake;${CMAKE_MODULE_PATH} CACHE STRING "CMake modules search path")
set(QML_IMPORT_PATH "${ToxerDesktop_SOURCE_DIR}/qml" CACHE STRING "Qt Creator extra qml import paths")

if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# setup version string
find_package(Git)
if(GIT_FOUND)
  execute_process(COMMAND ${GIT_EXECUTABLE} describe --tags --long --always --dirty=-d
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    RESULT_VARIABLE git_result
    OUTPUT_VARIABLE PROJECT_REVISION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_VARIABLE git_err
    )
  if (git_result EQUAL 0)
    message(STATUS "Toxer Git-Hash: ${PROJECT_REVISION}")
  else()
    message(FATAL_ERROR "Git error: ${git_err}")
  endif()
endif()
if(NOT PROJECT_REVISION)
  set(PROJECT_REVISION "DEVBUILD")
endif()
add_definitions(-DTOXER_VERSION="${PROJECT_REVISION}")

find_package(Qt6 REQUIRED COMPONENTS LinguistTools Quick)
find_package(sodium REQUIRED)
find_library(TOX_CORE toxcore REQUIRED)

qt_standard_project_setup()

# generate translation resource in build directory
file(GLOB TS_FILES "translation/*.ts")
qt_create_translation(QM_FILES ${TS_FILES})
if(QM_FILES)
  message("Translation created: ${QM_FILES}")
else()
  message(SEND_ERROR "Failed to create translation!"
    "\nMake sure the TS files exist in ${ToxerDesktop_SOURCE_DIR}/translation")
endif()
configure_file(translation/translation.qrc ${ToxerDesktop_BINARY_DIR} COPYONLY)

qt_add_executable(toxer
  src/main.cpp
  src/ToxerDesktop.cpp
  src/ToxerTheme.cpp
  )
qt_add_resources(toxer "masks"
  PREFIX "/masks"
  BASE "res/masks"
  FILES
    res/masks/circle.svg
)
qt_add_resources(toxer "images"
  PREFIX "/images"
  BASE "res/images"
  FILES
    res/images/dark/caps_lock.svg
    res/images/dark/contact.svg
    res/images/dark/group.svg
    res/images/dark/login_logo.svg
    res/images/dark/logout.svg
    res/images/dark/settings.svg
    res/images/dark/transfer.svg
    res/images/dark/add.svg
    res/images/dark/dot_invisible.svg
    res/images/dark/dot_away.svg
    res/images/dark/dot_busy.svg
    res/images/dark/dot_online.svg
    res/images/dark/dot_offline.svg
    res/images/dark/send_message.svg
    res/images/light/caps_lock.svg
    res/images/light/contact.svg
    res/images/light/group.svg
    res/images/light/login_logo.svg
    res/images/light/logout.svg
    res/images/light/settings.svg
    res/images/light/transfer.svg
    res/images/light/add.svg
    res/images/light/dot_away.svg
    res/images/light/dot_invisible.svg
    res/images/light/dot_offline.svg
    res/images/light/dot_busy.svg
    res/images/light/dot_online.svg
    res/images/light/send_message.svg
)
qt_add_qml_module(toxer
  URI ToxerDesktop
  VERSION 1.0
  QML_FILES
    # animations
    qml/animations/Pulsing.qml
    qml/animations/Ringing.qml
    # base
    qml/base/View.qml
    qml/base/ViewLoader.qml
    qml/base/MainViewBase.qml
    # controls
    qml/controls/Page.qml
    qml/controls/Text.qml
    qml/controls/Slider.qml
    # friend
    qml/friend/Info.qml
    qml/friend/Invite.qml
    qml/friend/List.qml
    qml/friend/Delegate.qml
    # messenger
    qml/messenger/View.qml
    qml/messenger/Message.qml
    qml/messenger/Delegate.qml
    qml/messenger/File.qml
    # settings
    qml/settings/Appearance.qml
    qml/settings/Overview.qml
    qml/settings/Tox.qml
    # MAIN
    qml/Main.qml
    qml/Start.qml
    qml/MainViewSplit.qml
    qml/CurrentProfile.qml
)

# configure executable target "toxer"
target_compile_options(toxer PUBLIC
  -W
  -Wall
  -fno-exceptions
  -fno-rtti
  -fvisibility=hidden
)
if(CMAKE_BUILD_TYPE STREQUAL "MinSizeRel")
  target_compiler_options(toxer PUBLIC
    -O3
  )
endif()
# FIXME: disable IPO as it crashes Qt6 for some reason
#include(CheckIPOSupported)
#check_ipo_supported(RESULT result)
#if(result)
#  target_compile_options(toxer PUBLIC
#    -flto
#  )
#  if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
#    target_compile_options(toxer PUBLIC
#      -fvisibility=hidden
#      -fsanitize=cfi
#    )
#  endif()
#  set_property(TARGET toxer PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
#endif()

set_property (TARGET toxer APPEND PROPERTY COMPILE_DEFINITIONS
  QT_NO_CAST_FROM_ASCII
  QT_NO_CAST_TO_ASCII
  QT_NO_CAST_FROM_BYTEARRAY
  )

# SUBDIR_TOXERCORE: build against subdirectoy
option(SUBDIR_TOXERCORE "Looks for libtoxercore sources in subdirectory ToxerCore." ON)
if(SUBDIR_TOXERCORE)
  message("Building with SUBDIR_TOXERCORE  ->  ToxerCore Developer Build")
  add_definitions(-DSUBDIR_TOXERCORE)
  add_subdirectory(ToxerCore)
  target_include_directories(toxer PRIVATE
    $<BUILD_INTERFACE:
      ${ToxerCore_SOURCE_DIR}/src
    >
  )
else()
  find_library(TOXERCORE toxercore REQUIRED)
endif()

target_link_libraries (toxer PRIVATE
  Qt6::Quick
  toxercore
  ${TOX_CORE}
  sodium
  )

install(TARGETS toxer
  RUNTIME DESTINATION bin COMPONENT Runtime
  LIBRARY DESTINATION lib COMPONENT Runtime
  )
install(FILES org.toxer.Toxer.desktop      DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/applications)
install(FILES org.toxer.Toxer.metainfo.xml DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/metainfo)
install(FILES org.toxer.Toxer.svg          DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/scalable/apps)
