<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Appearance</name>
    <message>
        <source>Single-Window layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Views are shown in a single window side by side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Slim Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Slim layout meant for small screens like in pocket-sized devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Base Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Font size: %1 pt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateProfile</name>
    <message>
        <source>Enter a Tox alias.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Location: %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter the profile&apos;s password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Retype the profile&apos;s password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportProfiles</name>
    <message>
        <source>&lt;h2&gt;This part of Toxer is work in progress!&lt;/h2&gt;&lt;p&gt;Want to help out?&lt;/p&gt;&lt;p&gt;Visit &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <source>Name: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Status Message: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tox-Key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove %1 from friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <source>Invite a friend to join your Tox network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter a Tox address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send Tox Invitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainViewSplit</name>
    <message>
        <source>Copy Tox-Address to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Appearance settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <source>Start Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import Profiles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectProfile</name>
    <message>
        <source>Enter the profile&apos;s password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>View</name>
    <message>
        <source>Info: File transfer has been cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File transfer not supported by Toxer -&gt; rejecting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type a message...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send a file to %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
