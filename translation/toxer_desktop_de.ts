<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>Appearance</name>
    <message>
        <source>Single-Window layout</source>
        <translation>Ein-Fenster Layout</translation>
    </message>
    <message>
        <source>Views are shown in a single window side by side</source>
        <translation>Alle Ansichten in einzelnem Fenster</translation>
    </message>
    <message>
        <source>Slim Layout</source>
        <translation>Schlankes Layout</translation>
    </message>
    <message>
        <source>Slim layout meant for small screens like in pocket-sized devices.</source>
        <translation>Schlankes Layout für kleine Bildschirme im &quot;Taschenformat&quot;.</translation>
    </message>
    <message>
        <source>Base Color</source>
        <translation>Grundfarbe</translation>
    </message>
    <message>
        <source>Hue</source>
        <translation>Farbton</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Sättigung</translation>
    </message>
    <message>
        <source>Lightness</source>
        <translation>Helligkeit</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation>Heller Hintergrund</translation>
    </message>
    <message>
        <source>Font size: %1 pt</source>
        <translation>Schriftgröße: %1 pt</translation>
    </message>
</context>
<context>
    <name>CreateProfile</name>
    <message>
        <source>Enter a Tox alias.</source>
        <translation>Gib einen Tox-Alias ein.</translation>
    </message>
    <message>
        <source>Location: %1/%2</source>
        <translation>Speicherort: %1/%2</translation>
    </message>
    <message>
        <source>Enter the profile&apos;s password.</source>
        <translation>Gib das Profil-Passwort ein.</translation>
    </message>
    <message>
        <source>Retype the profile&apos;s password.</source>
        <translation>Wiederhole das Profil-Passwort.</translation>
    </message>
    <message>
        <source>Create Profile</source>
        <translation>Profil Erzeugen</translation>
    </message>
</context>
<context>
    <name>ImportProfiles</name>
    <message>
        <source>&lt;h2&gt;This part of Toxer is work in progress!&lt;/h2&gt;&lt;p&gt;Want to help out?&lt;/p&gt;&lt;p&gt;Visit &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <source>Name: %1</source>
        <translation>Name: %1</translation>
    </message>
    <message>
        <source>Status Message: %1</source>
        <translation>Status-Nachricht: %1</translation>
    </message>
    <message>
        <source>Tox-Key:</source>
        <translation>Tox-Schlüssel:</translation>
    </message>
    <message>
        <source>To Clipboard</source>
        <translation>Nach Zwischenablage</translation>
    </message>
    <message>
        <source>Remove %1 from friends</source>
        <translation>Entferne Tox-Freund %1</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <source>Invite a friend to join your Tox network.</source>
        <translation>Lade einen Freund in dein Tox-Netzwerk ein.</translation>
    </message>
    <message>
        <source>Enter a Tox address.</source>
        <translation>Gib eine Tox-Adresse ein.</translation>
    </message>
    <message>
        <source>Send Tox Invitation</source>
        <translation>Tox-Einladung Senden</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>MainViewSplit</name>
    <message>
        <source>Copy Tox-Address to Clipboard</source>
        <translation>Tox-Adresse in Zwischenablage kopieren</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <source>Appearance</source>
        <translation>Erscheinungsbild</translation>
    </message>
    <message>
        <source>Appearance settings</source>
        <translation>Erscheinungsbild-Einstellungen</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <source>Start Profile</source>
        <translation>Starte Profil</translation>
    </message>
    <message>
        <source>New Profile</source>
        <translation>Neues Profil</translation>
    </message>
    <message>
        <source>Import Profiles</source>
        <translation>Importiere Profile</translation>
    </message>
</context>
<context>
    <name>SelectProfile</name>
    <message>
        <source>Enter the profile&apos;s password</source>
        <translation>Gib das Profil-Passwort ein</translation>
    </message>
    <message>
        <source>Load Profile</source>
        <translation>Profil Öffnen</translation>
    </message>
</context>
<context>
    <name>View</name>
    <message>
        <source>Info: File transfer has been cancelled.</source>
        <translation>Info: Dateitransfer wurde abgebrochen.</translation>
    </message>
    <message>
        <source>File transfer not supported by Toxer -&gt; rejecting.</source>
        <translation>Toxer unterstützt keinen Dateitransfer -&gt; lehne ab.</translation>
    </message>
    <message>
        <source>Send File</source>
        <translation>Datei Senden</translation>
    </message>
    <message>
        <source>Type a message...</source>
        <translation>Gib eine Nachricht ein …</translation>
    </message>
    <message>
        <source>Send a file to %1.</source>
        <translation>Datei an %1 senden.</translation>
    </message>
</context>
</TS>
