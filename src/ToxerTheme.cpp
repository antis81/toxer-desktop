#include "ToxerTheme.h"

using ToxerDesktop::Colors;
using ToxerDesktop::Theme;

Colors::Colors(QObject* parent)
  : QObject(parent)
{
  accent_.setBinding([this]{
    auto c = base_.value();
    return c.lighter(180);
  });

  base_hue_.setBinding([this]{
    return base_.value().hslHueF();
  });

  base_saturation_.setBinding([this]{
    return base_.value().hslSaturationF();
  });

  base_lightness_.setBinding([this]{
    return base_.value().lightnessF();
  });

  contrast_.setBinding([this]{
    auto c = base_.value();
    auto h = c.hslHueF();
    auto s = c.hslSaturationF();
    auto l = c.lightnessF();
    return QColor::fromHslF(qMin(1.0f, h + 0.2f), s, l + (l <= 0.5f ? 0.1f : -0.1f));
  });

  hover_.setBinding([this]{
    return accent_.value();
  });

  text_.setBinding([this]{
    return light_theme_.value() ? QColor::fromString(u"#333"_qs) : QColor::fromString(u"#ddd"_qs);
  });
}

Theme::Theme(QObject* parent)
  : QObject(parent)
  , colors_(new Colors(this))
{
}
