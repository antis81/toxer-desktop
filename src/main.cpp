/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2023 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QScreen>
#include <QThread>
#include <QTranslator>

#include "ToxerDesktop.h"
#include "ToxerTheme.h"

// FIXME: Always include headers from "toxercore" prefix.
#ifdef SUBDIR_TOXERCORE
#  include "ToxerQml.h"
#else
#  include "toxercore/ToxerQml.h"
#endif

using Toxer::Api;
using Toxer::Settings;
using ToxerDesktop::UiSettings;
using ToxerDesktop::Theme;

int main(int argc, char *argv[]) {
  QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);

  QGuiApplication app(argc, argv);
  app.thread()->setObjectName(u"MainThread"_qs);
  app.setOrganizationName(u"Tox"_qs);
  app.setApplicationName(u"Toxer"_qs);
  app.setApplicationVersion(QStringLiteral(TOXER_VERSION));

  QTranslator translator;
  { // setup translator
    auto qm_dir = u":/"_qs;
    auto qm_prefix = u"toxer_desktop"_qs;
    auto qm_separator = u"_"_qs;
    if (translator.load(QLocale(), qm_prefix, qm_separator, qm_dir)) {
      app.installTranslator(&translator);
    } else {
      qDebug("Looking for translations in: %s", qUtf8Printable(qm_dir));
      qInfo("No translation for locale %s.", qUtf8Printable(QLocale().name()));
    }
  }

  ToxerDesktop::registerQmlTypes();
  Toxer::registerQmlTypes();

  Api toxer;
  Theme toxer_theme;  // FIXME: Use standard Qt theme instead.

  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty(u"Toxer"_qs, &toxer);
  engine.rootContext()->setContextProperty(u"Style"_qs, &toxer_theme);



  engine.addImportPath(Toxer::pathTo(u"qml"_qs).toString());
  engine.load(Toxer::pathTo(u"qml/Main.qml"_qs));
  return app.exec();
}
