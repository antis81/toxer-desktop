import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import Toxer
import Toxer.Settings

Column {
  id: root
  width: 400
  height: 300
  spacing: 10

  ToxSettings { id: tox_settings }

  RowLayout {
    Button { text: qsTr("Reset"); onClicked: tox_settings.resetIp6(); }
    CheckBox {
      checked: tox_settings.ip6
      onCheckedChanged: tox_settings.ip6 = checked
      text: qsTr("Allow IP6 when available.")
      contentItem: Label {
        leftPadding: parent.indicator.width + 4
        text: parent.text
        verticalAlignment: Text.AlignVCenter
      }
    }
  }

  RowLayout {
    Button { text: qsTr("Reset"); onClicked: tox_settings.resetUdp(); }
    CheckBox {
      checked: tox_settings.udp
      onCheckedChanged: tox_settings.udp = checked
      text: qsTr("Enable UDP for Tox transfer.")
      contentItem: Text {
        leftPadding: parent.indicator.width + 4
        text: parent.text
        verticalAlignment: Text.AlignVCenter
      }
    }
  }

  Column {
    spacing: 10
    Label { text: qsTr("Proxy Settings") }
    RowLayout {
      Button { text: qsTr("Reset"); onClicked: tox_settings.resetProxyType(); }
      ComboBox {
        width: parent.width
        textRole: "text"
        model: ListModel {
          ListElement { value: ToxTypes.None; text: qsTr("no proxy"); description: qsTr("Do not use a proxy.") }
          ListElement { value: ToxTypes.SOCKS5; text: "SOCKS"; description: qsTr("SOCKS proxy for simple socket pipes.") }
          ListElement { value: ToxTypes.HTTP; text: "HTTP"; description: qsTr("HTTP proxy using CONNECT.") }
        }
        onActivated: {
          const item = model.get(index)
          tox_settings.setProxyType(item.value)
        }
        Component.onCompleted: {
          const v = tox_settings.proxyType
          for(let i = 0; i < count; i++) {
            if (model.get(i).value === v) {
              currentIndex = i
              return
            }
          }
        }
      }
    }
    RowLayout {
      spacing: 4
      Button { text: qsTr("Reset"); onClicked: tox_settings.resetProxyAddress(); }
      Label { Layout.alignment: Qt.AlignVCenter; text: qsTr("Proxy Address") }
      TextField {
        text: tox_settings.proxyAddress;
        onTextChanged: tox_settings.proxyAddress = text
        placeholderText: qsTr("disabled")
        Layout.fillWidth: true;
      }
    }
    RowLayout {
      spacing: 4
      Button { text: qsTr("Reset"); onClicked: tox_settings.resetProxyPort(); }
      Label { Layout.alignment: Qt.AlignVCenter; text: qsTr("Proxy Port") }
      SpinBox {
        value: tox_settings.proxyPort
        onValueChanged: tox_settings.proxyPort = value;
        editable: true
        stepSize: 5000
        to: 65535
      }
    }
  }
}
