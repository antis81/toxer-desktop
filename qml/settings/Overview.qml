import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts

import "../controls" as Controls

Controls.Page {
  id: root

  ListModel {
    id: menuModel

    ListElement { caption: qsTr("Appearance"); description: qsTr("Appearance settings"); page: "Appearance.qml" }
    ListElement { caption: qsTr("Tox"); description: qsTr("Tox network settings"); page: "Tox.qml" }
  }
  ColumnLayout {
    anchors.fill: parent

    Flow {
      id: menu
      Layout.fillWidth: true
      Layout.minimumHeight: 24

      Repeater {
        id: menuCtl
        model: menuModel

        Button {
          text: caption
          Accessible.description: description
          onClicked: { settingsPage.source = page }
        }
      }
    }
    Loader {
      id: settingsPage
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }
}
