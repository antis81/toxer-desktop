import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

import Toxer.Settings

import "../controls" as Controls

Controls.Page {
  id: root

  UiSettings { id: uiSettings }
  Column {
    width: 400
    height: 300
    spacing: 10

    Column {
      width: parent.width

      Label {
        text: qsTr("Base Color")
      }
      Controls.Slider {
        id: base_hue_slider
        width: root.availableWidth
        text: qsTr("Hue")
        value: Style.color.baseHue
        onMoved: ()=> {
          Style.color.baseHue = base_hue_slider.value
        }
      }
      Controls.Slider {
        id: base_saturation_slider
        width: root.availableWidth
        text: qsTr("Saturation")
        value: Style.color.baseSaturation
        onMoved: ()=> {
          Style.color.baseSaturation = base_saturation_slider.value
        }
      }
      Controls.Slider {
        id: base_lightness_slider
        width: root.availableWidth
        text: qsTr("Lightness")
        from: 0.1
        to: 0.9
        value: Style.color.baseLightness
        onMoved: ()=> {
          Style.color.baseLightness = base_lightness_slider.value
        }
      }
    }
    CheckBox {
      text: qsTr("Light Theme")
      contentItem: Label {
        leftPadding: parent.indicator.width + 4
        text: parent.text
        verticalAlignment: Text.AlignVCenter
      }
      checked: Style.color.lightTheme
      onCheckedChanged: { Style.color.lightTheme = checked; }
    }
    SpinBox {
      textFromValue: function() { return qsTr("Font size: %1 pt").arg(value); }
      from: 6
      value: Style.fontPointSize
      onValueChanged: { Style.fontPointSize = value; }
    }
  }
}
