/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Window 2.13

import base 1.0 as Base
import controls 1.0 as Controls
import style 1.0

Base.MainViewBase {
  id: root
  width: Math.min(280, Screen.width)
  height: Math.min(430, Screen.height)
  focus: true

  ButtonGroup {
    id: mainToolButtons
    exclusive: true
    onCheckedButtonChanged: {
      if (checkedButton === btnSettings) {
        viewLoader.source = "settings/Overview.qml"
      } else if (checkedButton === btnInviteFriend) {
        viewLoader.source = "friend/Invite.qml"
      } else {
        viewLoader.sourceComponent = undefined
      }
    }
  }
  ColumnLayout {
    anchors.fill: parent
    spacing: 0

    Rectangle {
      id: quickActions
      Layout.fillWidth: true
      Layout.preferredHeight: Math.max(root.height * 0.05, 32)
      color: Style.color.contrast
      clip: true

      RowLayout {
        anchors.fill: parent
        spacing: 0

        Item { Layout.fillWidth: true } // spacer
        Controls.RoundButton {
          id: btnInviteFriend
          Layout.alignment: Qt.AlignVCenter
          Layout.preferredHeight: quickActions.height - 8
          implicitWidth: height
          icon.source: Style.icon.add
          checkable: true
          ButtonGroup.group: mainToolButtons

          NumberAnimation {
            id: pushAni
            target: btnInviteFriend.contentItem
            property: "scale"
            duration: 200
            alwaysRunToEnd: true
          }
        }
        Controls.RoundButton {
          id: btnSettings
          Layout.alignment: Qt.AlignVCenter
          Layout.preferredHeight: quickActions.height - 8
          implicitWidth: height
          icon.source: Style.icon.settings
          checkable: true
          ButtonGroup.group: mainToolButtons

          NumberAnimation {
            id: rotAni
            target: btnSettings.contentItem
            property: "rotation"
            duration: 2000
            from: 0
            to: 360
            loops: Animation.Infinite
            running: btnSettings.checked
          }
        }
      }
    }
    CurrentProfile {
      id: selfView
      Layout.fillWidth: true
      Layout.minimumHeight: Math.max(root.height * 0.1)
      Layout.maximumHeight: 30
    }
    Base.ViewLoader {
      id: viewLoader
      Layout.fillWidth: true
      Layout.fillHeight: true
      defaultView: "friend/List.qml"

      Connections {
        target: viewLoader.item
        onClosing: { mainToolButtons.checkedButton = null }
      }
    }
  }
}
