import QtQuick
import QtQuick.Controls
import QtQuick.Templates as T

T.Slider {
  id: control
  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                          implicitHandleWidth + leftPadding + rightPadding)
  implicitHeight: 26;
  padding: 6
  stepSize: 0.01
  background: Rectangle {
    border.color: control.focus ? control.palette.highlight : control.enabled ? control.palette.mid : control.palette.midlight
    gradient: Gradient {
      GradientStop { position: 0.0; color: Style.color.base }
      GradientStop { position: 0.38; color: Style.color.contrast }
    }
    radius: height / 2
    clip: true

    Rectangle {
      id: value_indicator
      x: parent.border.width
      y: parent.border.width
      width: control.visualPosition * parent.width
      height: parent.height - 2 * parent.border.width
      radius: height / 2
      gradient: Gradient {
        GradientStop { position: 0.0; color: Style.color.contrast }
        GradientStop { position: 0.38; color: Style.color.base }
      }
    }
    Label {
      id: value_label
      anchors.centerIn: parent
    }
  }
  handle: null

  property alias text: value_label.text
}
