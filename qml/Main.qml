import QtQml
import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

ApplicationWindow {
  id: self
  width: pages.implicitWidth
  height: pages.implicitHeight
  x: (Screen.width - width) / 2
  y: (Screen.height - height) / 2
  visible: true

  Material.theme: Material.Dark // Style.color.lightTheme ? Material.light : Material.Dark
  Material.background: Style.color.base
  Material.foreground: Style.color.text
  Material.primary:    Style.color.contrast
  Material.accent:     Style.color.accent
  Material.elevation: 0

  Component {
    id: start_page
    Start {
      implicitWidth:  Math.min(500, Screen.width)
      implicitHeight: Math.min(600, Screen.height)
    }
  }

  Component {
    id: messenger_page
    MainViewSplit {
      implicitWidth:  Math.min(600, Screen.width)
      implicitHeight: Math.min(600, Screen.height)
    }
  }

  StackView {
    id: pages
    implicitWidth: currentItem.implicitWidth
    implicitHeight: currentItem.implicitHeight
    anchors.fill: parent
    initialItem: start_page
  }

  Connections {
    target: Toxer

    function onProfileChanged() {
      if (Toxer.hasProfile) {
        pages.push(messenger_page)
      } else {
        pages.pop(null)
      }
    }
  }
}
