import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Effects
import QtQuick.Layouts

import Toxer

Rectangle {
  id: root
  implicitWidth: 50
  implicitHeight: 30
  color: Style.color.contrast

  readonly property alias tox: tpq

  ToxProfileQuery {
    id: tpq
    onIsOnlineChanged: { status_light.source = statusIcon(); }
    onStatusChanged: { status_light.source = statusIcon(); }
    onUserNameChanged: { txt_name.text = userName; }
    onStatusMessageChanged: { txt_status_message.text = statusMessage; }

    function statusIcon() {
      return Style.getAvailability(isOnline(), statusInt())
    }
  }

  RowLayout {
    id: layout_tox_self
    anchors.fill: parent
    anchors.margins: parent.height * 0.05
    anchors.bottomMargin: 3
    spacing: 0

    Item {
      id: avatar_item
      Layout.fillHeight: true
      implicitWidth: height + status_light.width

      Image {
        id: img_avatar
        visible: false
        width: height
        height: parent.height
        fillMode: Image.PreserveAspectFit
        sourceSize: Qt.size(width, height)
        source: {
          const url = Toxer.avatarsUrl() + "/" + tpq.publicKeyStr().toUpperCase() + ".png"
          return Toxer.exists(url) ? url : Style.getIcon('noAvatar')
        }
      }
      MultiEffect {
        source: img_avatar
        anchors.fill: img_avatar
        maskEnabled: true
        maskSource: Image {
          source: "qrc:/masks/circle.svg"
        }
      }
      Image {
        id: status_light
        visible: true
        height: Math.max(img_avatar.height * 0.33, 10)
        width: height
        anchors.bottom: img_avatar.bottom
        anchors.right: img_avatar.right
        anchors.rightMargin: -(height / 2)
        fillMode: Image.PreserveAspectFit
        sourceSize: Qt.size(width, height)
        source: tpq.statusIcon()
      }
    }
    Column {
      id: col_tox_info
      Layout.minimumWidth: 20
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignTop

      Label {
        id: txt_name
        width: parent.width
        font.bold: true
        text: tpq.userName()
      }
      Label {
        id: txt_status_message
        width: parent.width
        text: tpq.statusMessage()
      }
    }
  }
}
