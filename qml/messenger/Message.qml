import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

RowLayout {
  id: root

  readonly property bool isMe: pk === toxProfile.publicKeyStr()

  Label {
    id: msg_time
    Layout.alignment: Qt.AlignTop
    text: Qt.formatTime(new Date(ts), "hh:mm:ss")
  }
  Rectangle {
    id: msg_container
    Layout.fillWidth: true
    implicitHeight: msg_layout.height
    color: root.isMe ? "#5E97EB" : "#424954"
    radius: 4

    Row {
      id: msg_layout
      layoutDirection: root.isMe ? Qt.LeftToRight : Qt.RightToLeft

      Item {
        id: pic_container
        height: Math.min(Style.fontPointSize * 2, 20)
        width: height

        Image {
          id: pic
          anchors.fill: parent
          visible: false
          sourceSize: Qt.size(width, height)
          source: {
            const url = Toxer.avatarsUrl() + "/" + pk.toUpperCase() + ".png"
            return Toxer.exists(url) ? url : Style.getIcon('noAvatar')
          }
        }
        Rectangle {
          id: pic_mask
          anchors.fill: pic
          color: "blue"
          radius: width / 2
          clip: true
          visible: false
        }
        // OpacityMask {
        //   anchors.fill: pic_mask
        //   source: pic
        //   maskSource: pic_mask
        // }
      }
      Text {
        id: msg_text
        padding: 4
        text: message
        color: "#F6F6F9"
        width: msg_container.width - pic_container.width
        maximumLineCount: 50
        wrapMode: Text.WordWrap
        elide: Text.ElideNone
      }
    }
  }
}
