import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs

import Toxer

import "../controls" as Controls
import "../friend" as Friend

Controls.Page {
  id: root
  width: 320
  height: 600
  focus: true
  Component.onCompleted: { txt_message_input.forceActiveFocus(); }

  property alias friendNo: my_friend.fno

  function sendMessage() {
    const message = txt_message_input.text
    if (message) {
      my_friend.sendMessage(message)
      messageModel.addMessageItem(toxProfile.publicKeyStr(), message)
      txt_message_input.clear()
    }
  }

  ToxProfileQuery { id: toxProfile }
  ToxFriend {
    id: my_friend
    onMessage: { messageModel.addMessageItem(my_friend.pkStr(), message); }
  }
  ListModel {
    id: messageModel
    Component.onCompleted: {
      addMessageItem("", "Note from Toxer:\nToxer messaging is work in progress! The messages you send or receive from friends will not be saved in any way and be lost when leaving the chat view (e.g. by chosing another friend).\n\nThe Toxer developers!")
    }

    function addMessageItem(pk, message) {
      const ts = new Date().getTime()
      append({ pk: pk, ts: ts, message: message })
    }
  }
  Connections {
    target: Toxer

    function onFileCancelled() { messageModel.addMessageItem(toxProfile.publicKeyStr(), qsTr("Info: File transfer has been cancelled.")); }
    function onFileRequest() {
      const str = qsTr("File transfer not supported by Toxer -> rejecting.")
      my_friend.sendMessage(str)
      messageModel.addMessageItem(toxProfile.publicKeyStr(), str)
      my_friend.cancelFile(fileNo)
    }
  }

  contentItem: SplitView {
    id: v_split
    orientation: Qt.Vertical

    ListView {
      id: message_box
      SplitView.fillWidth: true
      SplitView.fillHeight: true
      spacing: 2
      clip: true
      model: messageModel
      delegate: Message { width: message_box.width }
      currentIndex: messageModel.count - 1 // auto-scroll to last item
    }
    Column {
      spacing: 8

      Friend.Delegate {
        id: friend_info
        height: 26
        name.text: my_friend.name
        statusMessage.visible: true
        statusMessage.text: my_friend.statusMessage
        avatar.source: {
          const url = Toxer.avatarsUrl() + "/" + my_friend.pkStr().toUpperCase() + ".png"
          return Toxer.exists(url) ? url : Style.getIcon('noAvatar')
        }
        statusLight.source: Style.getAvailability(my_friend.isOnline, my_friend.availabilityInt())
      }
      Row {
        id: row_input
        width: parent.width
        spacing: 0
        clip: true

        RoundButton {
          id: btn_send_file
          Accessible.defaultButton: true
          enabled: !fileDialog.visible
          text: qsTr("Send File")
          onClicked: { fileDialog.visible = true; }
        }
        ScrollView {
          id: input_view
          height: Math.min(contentHeight, v_split.height / 1.3)
          width: row_input.width - btn_send_message.width - btn_send_file.width

          TextArea {
            id: txt_message_input
            placeholderText: qsTr("Type a message...")
            wrapMode: TextEdit.WordWrap
          }
        }
        RoundButton {
          id: btn_send_message
          Accessible.defaultButton: true
          enabled: my_friend.isOnline && txt_message_input.text
          display: AbstractButton.IconOnly
          icon.source: Style.getIcon('sendMessage')
          icon.color: enabled ? "transparent" : Style.color.contrast
          flat: true
          onClicked: { root.sendMessage(); }
        }
      }
    }
  }
  FileDialog {
    id: fileDialog
    title: qsTr("Send a file to %1.").arg(my_friend.name)
    fileMode: FileDialog.OpenFile
    currentFolder: StandardPaths.standardLocations(StandardPaths.HomeLocation)[0]
    onAccepted: my_friend.sendFile(fileUrl)
  }
}
