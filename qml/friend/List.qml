import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material

import Toxer

Item {
  id : root

  property bool compactMode: false
  readonly property alias selectedFriend: friends_view.currentIndex

  ToxFriends {
    id: friends

    function statusIcon(friend_no) {
      return Style.getAvailability(isOnline(friend_no), statusInt(friend_no))
    }
  }
  Menu {
    id: friendMenu

    property int friendNo: -1

    MenuItem { text: "Delete Friend" ; onTriggered: { friends.remove(friendMenu.friendNo); } }
  }
  ListView {
    id: friends_view
    anchors.fill: parent
    model: friends.list.slice(0)
    spacing: root.compactMode ? 1 : 2
    clip: true
    focus: true
    highlightFollowsCurrentItem: false
    highlight: Rectangle {
      visible: friends_view.currentItem ? true : false
      width: friends_view.currentItem ? friends_view.currentItem.width : 0
      height: friends_view.currentItem ? friends_view.currentItem.height : 0
      border.color: "#40FFFFFF"
      color: "#33000000"
      y: friends_view.currentItem ? friends_view.currentItem.y : 0
      z: friends_view.currentItem ? friends_view.currentItem.z + 1 : 0

      Behavior on y {
        NumberAnimation { duration: 120 }
      }
    }
    delegate: Delegate {
      id: friend_delegate
      width: friends_view.width
      clip: true
      txt_name { text: friends.name(modelData) }
      txt_status_message {
        text: friends.statusMessage(modelData)
        visible: !root.compactMode
      }
      img_avatar.source: {
        const url = Toxer.avatarsUrl() + "/" + friends.publicKeyStr(modelData).toUpperCase() + ".png"
        return Toxer.exists(url) ? url : Style.getIcon('noAvatar')
      }
      img_status_light.source: friends.statusIcon(modelData)

      MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: (mouse)=> {
          friends_view.currentIndex = index
          if (mouse.button === Qt.RightButton) {
            friendMenu.friendNo = modelData
            friendMenu.popup()
          }
        }
        onDoubleClicked: (mouse)=> {
          if (viewLoader) {
            viewLoader.setSource("qrc:/ToxerDesktop/qml/messenger/View.qml", { friendNo: modelData })
          } else {
            console.warn("No ViewLoader assigned to View item " + root)
          }
        }
      }
      Connections {
        target: friends

        function onIsOnlineChanged(index, online) {
          if (index === modelData) {
            friend_delegate.img_status_light.source = friends.statusIcon(modelData)
          }
        }
        function onMessage(index, message) {
          if (index === modelData) {
            friend_delegate.unreadMessages++
          }
        }
        function onStatusChanged(index, status) {
          if (index === modelData) {
            friend_delegate.img_status_light.source = friends.statusIcon(modelData)
          }
        }
        function onNameChanged(index, name) {
          if (index === modelData) {
            friend_delegate.txt_name.text = name
          }
        }
        function onStatusMessageChanged(index, statusMessage) {
          if (index === modelData) {
            friend_delegate.txt_status_message.text = statusMessage
          }
        }
      }
    }
  }
}
