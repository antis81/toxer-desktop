import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts

import Toxer
import Toxer.Desktop

import "../controls" as Controls

Controls.Page {
  id: root

  property int friendNo: -1

  Clipboard { id: clipboard }
  ToxFriends { id: friends }
  ColumnLayout {
    id: col
    anchors.fill: parent

    Image {
      Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
      sourceSize: Qt.size(root.height / 5, root.height / 5)
      source: {
        const url = Toxer.avatarsUrl() + "/" + friends.publicKeyStr(friendNo).toUpperCase() + ".png"
        return Toxer.exists(url) ? url : Style.getIcon('noAvatar')
      }
    }
    Label {
      Layout.alignment: Qt.AlignCenter
      text: qsTr("Name: %1").arg(friends.name(friendNo))
    }
    Label {
      Layout.alignment: Qt.AlignCenter
      text: qsTr("Status Message: %1").arg(friends.statusMessage(friendNo))
    }
    RowLayout {
      id: rowToxId
      Label {
        Layout.alignment: Qt.AlignCenter
        text: qsTr("Tox-Key:")
        elide: Text.ElideNone
      }
      Label {
        Layout.alignment: Qt.AlignCenter
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        text: friends.publicKeyStr(friendNo)
        elide: Text.ElideMiddle
      }
      RoundButton {
        Layout.alignment: Qt.AlignCenter
        text: qsTr("To Clipboard")
        onClicked: {
          clipboard.setText(friends.publicKeyStr(friendNo))
        }
      }
    }
    Button {
      Layout.alignment: Qt.AlignCenter
      text: qsTr("Remove %1 from friends").arg(friends.name(friendNo))
    }
    Button {
      Layout.alignment: Qt.AlignCenter
      text: qsTr("Close")
      onClicked: { closing() }
    }
  }
}
