import QtQuick
import QtQuick.Controls.Material

import Toxer.Settings

import "../controls" as Controls

Controls.Page {
  id: root

  readonly property UiSettings uiSettings: UiSettings {}
}
