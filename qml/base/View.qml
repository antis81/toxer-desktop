import QtQuick

import '../controls' as Controls

Controls.Page {
  id: root
  background: null

  property ViewLoader viewLoader: null

  signal closing
}
